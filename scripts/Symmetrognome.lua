local dev,dev_spawn,dev_z5,dev_invincible
-- dev=true
dev_spawn=dev
dev_z5=dev
dev_invincible=dev

local attack = require "necro.game.character.Attack"
local action = require "necro.game.system.Action"
local commonSpell = require "necro.game.data.spell.CommonSpell"
local components = require "necro.game.data.Components"
local customEntities = require "necro.game.data.CustomEntities"
local damage = require "necro.game.system.Damage"
local event = require "necro.event.Event"
local spellItem = require "necro.game.item.SpellItem"
local voice = require "necro.audio.Voice"
local move = require "necro.game.system.Move"
local object = require "necro.game.object.Object"
local player = require "necro.game.character.Player"
local invincibility = require "necro.game.character.Invincibility"
local confusion = require "necro.game.character.Confusion"
local spell = require "necro.game.spell.Spell"
local ecs = require "system.game.Entities"

components.register {
  marker={},
}

if dev then
  event.levelLoad.add("spawn", {order="entities"},function (ev)
    if dev_spawn then
      object.spawn("Metrognome",2,0)
    end

    if dev_invincible then
      for _,p in ipairs(player.getPlayerEntities()) do
        invincibility.activate(p,999)
      end
    end
  end)
end
if dev_z5 then
  event.levelGenerate.add("generateProceduralLevel", {sequence=-1}, function (ev)
    -- all zones are zone 5
    local zone = 5
    ev.level = (ev.level%4) + (zone-1)*4
  end)
end

event.objectTakeDamage.add("doSymmetrognome", {order="spell", filter="symmetrognome_marker"}, function(ev)
  local player=ev.attacker
  if player and player:hasComponent"playableCharacter" and ev.type == damage.Type.PHYSICAL then
    move.absolute(player,0,0,bit.bor(move.Flag.RESET_PREVIOUS_POSITION, move.Flag.VOCALIZE))
  end
end)

-- make all metrognomes terrible
event.entitySchemaLoadNamedEnemy.add("makeMetrognomesGood", {key="metrognome"}, function (ev)
  if dev then dbg("symmetrognome created") end
  ev.entity.symmetrognome_marker = {}
end)
